*** Settings ***
Documentation
...    Basic Test for Google.com in headless mode

Library    Collections
Library    DateTime
Library    OperatingSystem
Library    SeleniumLibrary
Library    String


Suite Setup    Setup Environment and Open Browser
Suite Teardown    Close All Browsers

*** Variables ***
${BROWSER}       HeadlessChrome
${SITE_URL}      https://www.google.com
${SITE_NAME}     Google

*** Test Cases ***

Title Test
    Title Should Be    ${SITE_NAME}

*** Keywords ***
Open Chrome Browser To URL
    [Documentation]    Open Chrome browser and navigate to URL with browser options set
    [Tags]  open_chrome_browser
    ${browserOptions}    Run Keyword If    'Headless' in '${BROWSER}'    Set Headless Chrome Options
    Create Webdriver    Chrome    chrome_options=${browserOptions}
    Go To    ${SITE_URL}
    Maximize Browser Window

Browser timeout and speed
    [Documentation]
    ...    Set browser timeout and speed
    Set Selenium Timeout   30s
    Set Selenium Speed   0s

Set Headless Chrome Options
    [Documentation]
    ...     Set the Chrome options for running in headless mode.  Restrictions do not apply to headless mode.
    [Tags]   headless_chrome_options
    ${chromeOptions}    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys
    Call Method    ${chromeOptions}    add_argument    test-type
    Call Method    ${chromeOptions}    add_argument    --headless
    Call Method    ${chromeOptions}    add_argument    --disable-extensions
    Call Method    ${chromeOptions}    add_argument    --disable-gpu
    Call Method    ${chromeOptions}    add_argument    --disable-dev-shm-usage
    Call Method    ${chromeOptions}    add_argument    --no-sandbox
    [Return]  ${chromeOptions}

Setup Environment and Open Browser
    [Documentation]
    ...    This keyword will establish the environment variables and open a browser
    [Tags]    simple_test

    Open Chrome Browser To URL
    Browser timeout and speed


