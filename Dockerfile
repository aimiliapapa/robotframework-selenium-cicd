FROM ubuntu:latest
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y python3-pip
RUN pip install robotframework
RUN pip install python-dotenv
RUN pip install environs
RUN pip install robotframework-selenium2library
RUN pip install robotframework-seleniumlibrary
RUN pip install robotframework-imaplibrary2
RUN apt-get update
RUN apt-get install -y xvfb
RUN apt-get install -y zip
RUN apt-get install -y wget
RUN apt-get install -y ca-certificates
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y libnss3-dev libasound2 libxss1 libappindicator3-1 libindicator7 gconf-service libgconf-2-4 libpango1.0-0 xdg-utils fonts-liberation libgbm1
RUN apt-get install -y libcurl4
RUN CHROMEDRIVER_VERSION=`wget --no-verbose --output-document - https://chromedriver.storage.googleapis.com/LATEST_RELEASE` && \
    wget --no-verbose --output-document chromedriver_linux64.zip http://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip
RUN unzip -qq chromedriver_linux64.zip
RUN chmod +x chromedriver
RUN ln chromedriver /usr/local/bin/chromedriver
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt --fix-broken install
RUN dpkg -i google-chrome*.deb



